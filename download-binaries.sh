mkdir -p {rpms,bins}
pushd rpms
curl -L -O https://apache.bintray.com/aurora/centos-7/aurora-tools-0.16.0-1.el7.centos.aurora.x86_64.rpm
curl -L -O https://apache.bintray.com/aurora/centos-7/aurora-scheduler-0.16.0-1.el7.centos.aurora.x86_64.rpm
curl -L -O https://apache.bintray.com/aurora/centos-7/aurora-executor-0.16.0-1.el7.centos.aurora.x86_64.rpm
curl -L -O http://repos.mesosphere.com/el/7/x86_64/RPMS/mesos-1.0.1-2.0.93.centos701406.x86_64.rpm
curl -L -O ftp://mirror.switch.ch/pool/4/mirror/centos/7.2.1511/os/x86_64/Packages/libevent-2.0.21-4.el7.x86_64.rpm
curl -L -O https://archive.cloudera.com/cdh5/redhat/7/x86_64/cdh/5.7.1/RPMS/x86_64/zookeeper-3.4.5+cdh5.7.1+93-1.cdh5.7.1.p0.15.el7.x86_64.rpm
curl -L -O https://archive.cloudera.com/cdh5/redhat/7/x86_64/cdh/5.7.1/RPMS/x86_64/zookeeper-server-3.4.5+cdh5.7.1+93-1.cdh5.7.1.p0.15.el7.x86_64.rpm
curl -L -O http://repos.mesosphere.com/el/7/x86_64/RPMS/marathon-1.3.6-1.0.540.el7.x86_64.rpm
popd

pushd bins
  curl -L -O https://github.com/mesosphere/mesos-dns/releases/download/v0.6.0/mesos-dns-v0.6.0-linux-amd64
  curl -L -O https://github.com/containous/traefik/releases/download/v1.0.3/traefik_linux-amd64
popd
